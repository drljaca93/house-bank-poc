package avro

import domain._
import com.sksamuel.avro4s.{AvroSchema, RecordFormat}
import com.softwaremill.sttp._
import com.softwaremill.sttp.HttpURLConnectionBackend


/**
  * Avro test methods
  */
object AvroUtils {
    
    def main(args: Array[String]): Unit = {
        
        storeSchemaInSchemaRegistry
        getSchemaRegistries
        
        
    }
    
    /**
      * Helper method
      * Stores CreditCardBalance schema to schema registry
      */
    def storeSchemaInSchemaRegistry(): Unit = {
        
        val schema = AvroSchema[CreditCardBalance]
        val adjustedSchema = "{\"schema\":\"" + schema.toString.replace("\"", "\\\"") + "\"}";

        
        val request = sttp
            .body(adjustedSchema.toString).contentType("application/vnd.schemaregistry.v1+json")
            .post(uri"http://localhost:8081/subjects/CreditCardBalance-value/versions")
        
        
        implicit val backend = HttpURLConnectionBackend()
        val response = request.send()
        
        // response.unsafeBody: by default read into a String
        println(response)
    }
    
    
    /**
      * Helper method
      * Reads CreditCardBalance schema from schema registry
      */
    def getSchemaRegistries(): Unit = {
        
        //val sort: Option[String] = None
        //val query = "http language:scala"
        
        // the `query` parameter is automatically url-encoded
        // `sort` is removed, as the value is not defined
        val request = sttp.get(uri"http://localhost:8081/subjects/CreditCardBalance/versions/latest")
        
        implicit val backend = HttpURLConnectionBackend()
        val response = request.send()
        
        // response.unsafeBody: by default read into a String
        println(response.unsafeBody)
    }
    
}
