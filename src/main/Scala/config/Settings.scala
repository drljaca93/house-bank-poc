package config

import com.typesafe.config.ConfigFactory

object Settings {
    
    //private val config = ConfigFactory.load()
    
    private val config = ConfigFactory.parseResources("application.conf")
    
    /**
      * DataIngest module settings
      */
    object DataIngest {
        private val dataIngest = config.getConfig("dataIngest")
        
        lazy val hdfsPath = dataIngest.getString("hdfs_path")
        lazy val ccBalanceFilesPath = dataIngest.getString("ccBalanceFilesPath")
        lazy val instalPaymentsFilesPath = dataIngest.getString("instalPaymentsFilesPath")
        lazy val applicationsFilesPath = dataIngest.getString("applicationsFilesPath")
        
    }
    
    /**
      * Kafka settings
      */
    object Kafka {
        private val kafka = config.getConfig("kafkaSettings")
    
        lazy val bootstraperServers = kafka.getString("bootstraperServers")
        lazy val checkpointsLocation = kafka.getString("checkpointsLocation")
    }
    
    /**
      * Spark settings
      */
    object Spark {
        private val spark = config.getConfig("sparkSettings")
        
        lazy val master = spark.getString("master")
    }
    
    
    
}

/**
  * spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.2 --class dataTransform.CCBalanceTransformer
  * --deploy-mode cluster  house-bank-poc-1.0-SNAPSHOT-shaded.jar
  *
  */
