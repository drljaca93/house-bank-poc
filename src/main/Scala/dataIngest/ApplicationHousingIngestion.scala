package dataIngest

import java.util.Properties
import config.Settings
import domain._
import kafka.KafkaSink
import org.apache.kafka.clients.producer.{Callback, ProducerConfig, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.spark.sql.SparkSession
import utils.{LoggerUtils, SparkUtils}
import com.owlike.genson.defaultGenson._

object ApplicationHousingIngestion {
    
    
    def main(args: Array[String]): Unit = {
        
        LoggerUtils.enableOnlyErrorLogging
        readApplicationHousingAndSendToKafka()
        
    }
    
    /**
      * Reads installment payments and sends them to Kafka topic
      */
    
    def readApplicationHousingAndSendToKafka(): Unit = {
        val spark = SparkSession.builder()
            .master(Settings.Spark.master)
            .appName("dataIngest1")
            .getOrCreate()
        
        import spark.implicits._
        //Read data from csv file
        val inputData = spark
            .readStream
            .format("csv")
            .option("header", "true")
            .schema(FileSchemas.applicationSchema)
            .load(Settings.DataIngest.applicationsFilesPath)
        
        //transform data to DataSet[ApplicationHousing]
        val inputDataHousing = inputData
            .withColumn("skIdCurr", $"SK_ID_CURR".cast("long"))
            .withColumn("apartmentsAvg", $"APARTMENTS_AVG".cast("double"))
            .withColumn("basementAreaAvg", $"BASEMENTAREA_AVG".cast("double"))
            .withColumn("yearsBeginXPluatationAvg", $"YEARS_BEGINEXPLUATATION_AVG".cast("double"))
            .withColumn("yearsBuildAvg", $"YEARS_BUILD_AVG".cast("double"))
            .withColumn("commonAreaAvg", $"COMMONAREA_AVG".cast("double"))
            .withColumn("elevatorsAvg", $"ELEVATORS_AVG".cast("double"))
            .withColumn("entrancesAvg", $"ENTRANCES_AVG".cast("double"))
            .withColumn("floorsmaxAvg", $"FLOORSMAX_AVG".cast("double"))
            .withColumn("floorsMinAvg", $"FLOORSMIN_AVG".cast("double"))
            .withColumn("landAreaAvg", $"LANDAREA_AVG".cast("double"))
            .withColumn("livingApartmentsAvg", $"LIVINGAPARTMENTS_AVG".cast("double"))
            .withColumn("livingAreaAvg", $"LIVINGAREA_AVG".cast("double"))
            .withColumn("nonLivingApartmentsAvg", $"NONLIVINGAPARTMENTS_AVG".cast("double"))
            .withColumn("nonLivingAreaAvg", $"NONLIVINGAREA_AVG".cast("double"))
            .withColumn("apartmentsMode", $"APARTMENTS_MODE".cast("double"))
            .withColumn("basementAreaMode", $"BASEMENTAREA_MODE".cast("double"))
            .withColumn("codeGender", $"CODE_GENDER".cast("double"))
            .withColumn("nameIncomeType", $"NAME_INCOME_TYPE".cast("string"))
            .withColumn("nameEducationType", $"NAME_EDUCATION_TYPE".cast("string"))
            .withColumn("nameFamilyStatus", $"NAME_FAMILY_STATUS".cast("string"))
            .withColumn("occupationType", $"OCCUPATION_TYPE".cast("string"))
            .withColumn("cntFamMembers", $"CNT_FAM_MEMBERS".cast("int"))
            .as[ApplicationHousing]
        
        //write housing to kafka
        val kafkaHousingDS = inputDataHousing.map(ah => {
            (ah.skIdCurr, toJson(ah))
        })
        
        //write data to kafka topic
        val writer = kafkaHousingDS.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "application-housing")
            .option("checkpointLocation", "checkpoints/applicationHousingIngestion_checkpoints")
            .start()
        
        writer.awaitTermination()
        
    }
    
    /**
      * Writes data using KafkaSink and RDDs
      */
    def readCCBalanceAndSendToKafkaRDD(): Unit = {
        LoggerUtils.enableOnlyErrorLogging
        val sparkContext = SparkUtils.getSparkContext("dataIngest")
        
        val kafkaProperties = new Properties()
        kafkaProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, Settings.Kafka.bootstraperServers)
        kafkaProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
        kafkaProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
        
        val kafkaSink = sparkContext.broadcast(KafkaSink(kafkaProperties))
        
        //move file location to configuraiton
        //make it generic for all types
        //hdfs://lambda-pluralsight:9000/
        val inputDataRDD = sparkContext.textFile("hdfs://127.0.0.1:9000/user/maria_dev/BankSet/CreditCardBalance/credit_card_balance_1_1.csv")
        
        //write function to map credit_card_balance line to CreditCardBalance object
        inputDataRDD.foreach(line => {
            val splits = line.split(",")
            val record = new ProducerRecord[String, String]("credit-card-balance", splits(0), line)
            kafkaSink.value.send(record, new Callback {
                override def onCompletion(recordMetadata: RecordMetadata, e: Exception): Unit = {
                    println("Message offset: " + recordMetadata.offset() + " on topic: " + recordMetadata.topic())
                }
            })
        })
    }
    
}
