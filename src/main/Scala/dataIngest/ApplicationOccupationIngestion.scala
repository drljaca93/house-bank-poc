package dataIngest

import config.Settings
import domain.{ApplicationOccupation, FileSchemas}
import org.apache.spark.sql.SparkSession
import com.owlike.genson.defaultGenson._
import utils.LoggerUtils


object ApplicationOccupationIngestion {
    
    def main(args: Array[String]): Unit = {
        LoggerUtils.enableOnlyErrorLogging
        readApplicationOccupationAndSendToKafka()
    }
    
    def readApplicationOccupationAndSendToKafka(): Unit = {
        val spark = SparkSession.builder()
            .master(Settings.Spark.master)
            .appName("dataIngest")
            .getOrCreate()
        
        import spark.implicits._
        
        //Read data from csv file
        val inputData = spark
            .readStream
            .format("csv")
            .option("header", "true")
            .schema(FileSchemas.applicationSchema)
            .load(Settings.DataIngest.applicationsFilesPath)
        
        //transform data to DataSet[ApplicationHousing]
        val inputOccupation = inputData
            .withColumn("skIdCurr", $"SK_ID_CURR".cast("long"))
            .withColumn("apartmentsAvg", $"APARTMENTS_AVG".cast("double"))
            .withColumn("basementAreaAvg", $"BASEMENTAREA_AVG".cast("double"))
            .withColumn("yearsBeginXPluatationAvg", $"YEARS_BEGINEXPLUATATION_AVG".cast("double"))
            .withColumn("yearsBuildAvg", $"YEARS_BUILD_AVG".cast("double"))
            .withColumn("commonAreaAvg", $"COMMONAREA_AVG".cast("double"))
            .withColumn("elevatorsAvg", $"ELEVATORS_AVG".cast("double"))
            .withColumn("entrancesAvg", $"ENTRANCES_AVG".cast("double"))
            .withColumn("floorsmaxAvg", $"FLOORSMAX_AVG".cast("double"))
            .withColumn("floorsMinAvg", $"FLOORSMIN_AVG".cast("double"))
            .withColumn("landAreaAvg", $"LANDAREA_AVG".cast("double"))
            .withColumn("livingApartmentsAvg", $"LIVINGAPARTMENTS_AVG".cast("double"))
            .withColumn("livingAreaAvg", $"LIVINGAREA_AVG".cast("double"))
            .withColumn("nonLivingApartmentsAvg", $"NONLIVINGAPARTMENTS_AVG".cast("double"))
            .withColumn("nonLivingAreaAvg", $"NONLIVINGAREA_AVG".cast("double"))
            .withColumn("apartmentsMode", $"APARTMENTS_MODE".cast("double"))
            .withColumn("basementAreaMode", $"BASEMENTAREA_MODE".cast("double"))
            .withColumn("codeGender", $"CODE_GENDER".cast("string"))
            .withColumn("nameIncomeType", $"NAME_INCOME_TYPE".cast("string"))
            .withColumn("nameEducationType", $"NAME_EDUCATION_TYPE".cast("string"))
            .withColumn("nameFamilyStatus", $"NAME_FAMILY_STATUS".cast("string"))
            .withColumn("occupationType", $"OCCUPATION_TYPE".cast("string"))
            .withColumn("cntFamMembers", $"CNT_FAM_MEMBERS".cast("int"))
            .as[ApplicationOccupation]
        
        //write housing to kafka
        val kafkaOccupationDS = inputOccupation.map(ao => {
            (ao.skIdCurr, toJson(ao))
        })
        
        //write data to kafka topic
        val writer = kafkaOccupationDS.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "application-occupation")
            .option("checkpointLocation", "checkpoints/appOccupationIngestion_checkpoints")
            .start()
        
        writer.awaitTermination()
        
    }
}
