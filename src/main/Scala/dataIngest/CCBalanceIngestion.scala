package dataIngest

import config.Settings
import domain.{CreditCardBalance, FileSchemas}
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}
import com.owlike.genson.defaultGenson._
import com.sksamuel.avro4s._
import com.twitter.bijection.Injection
import com.twitter.bijection.avro.GenericAvroCodecs
import org.apache.avro.generic.GenericRecord
import org.apache.spark.sql
import utils.LoggerUtils
import za.co.absa.abris.avro.read.confluent.SchemaManager


object CCBalanceIngestion {
    
    def main(args: Array[String]): Unit = {
        LoggerUtils.enableOnlyErrorLogging
        
        val spark = SparkSession.builder()
            .master(Settings.Spark.master)
            .appName("dataIngest")
            .getOrCreate()
        
        val inputDataDS = readCCBalance(spark)
        //transformAndWriteToKafka(inputDataDS)
        transformAndWriteToKafkaAvroSchemaRegistry(spark, inputDataDS)
    }
    
    /**
      * Reads ccBalance data from input files and
      * sends it to kafka topic using spark kafka connector
      */
    def readCCBalance(spark: SparkSession): Dataset[CreditCardBalance] = {
        
        
        import spark.implicits._
        //Read data from csv file
        
        //implicit val ccBalanceEncoder = Encoders.product[CreditCardBalance]
        
        val inputData = spark
            .readStream
            .format("csv")
            .option("header", "true")
            .schema(FileSchemas.ccBalanceSchema)
            .load(Settings.DataIngest.ccBalanceFilesPath)
        
        //transform data to DataSet[CreditCardBalance]
        val inputDataDS = inputData
            .withColumn("skIdPrev", $"SK_ID_PREV".cast("long"))
            .withColumn("skIdCurr", $"SK_ID_CURR".cast("long"))
            .withColumn("monthsBalance", $"MONTHS_BALANCE".cast("int"))
            .withColumn("amtBalance", $"AMT_BALANCE".cast("double"))
            .as[CreditCardBalance]
        
       // inputDataDS.printSchema()
        //inputDataDS.show(10)
        
        inputDataDS
        
    }
    
    /**
      * Transforms dataset to kafka applicable format and
      * sends data to kafka topic credit-card-balance
      *
      * @param inputDataDS Dataset[CreditCardBalance]
      */
    def transformAndWriteToKafka(inputDataDS: Dataset[CreditCardBalance]): Unit = {
        
        implicit val kafkaRecordEncoder: sql.Encoder[(Long, String)] = Encoders.product[(Long, String)]
        //transform data to kafka applicable format
        val kafkaDS = inputDataDS.map(ccb => {
            (ccb.skIdCurr, toJson(ccb))
        })
        
        //write data to kafka topic
        val writer = kafkaDS.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "credit-card-balance")
            .option("checkpointLocation", "checkpoints/ccBalanceIngest_checkpoints")
            .start()
        
        writer.awaitTermination()
        
    }
    
    
    /**
      * Transform dataset to kafka applicable format using Avro and schema registry
      * and sends data to kafka topic redit_card_balance_avro
      * see https://github.com/confluentinc/schema-registry for more examples
      *
      * @param inputDataDS Dataset[CreditCardBalance]
      */
    def transformAndWriteToKafkaAvroSchemaRegistry(spark: SparkSession, inputDataDS: Dataset[CreditCardBalance]): Unit = {
        import spark.implicits._
        import za.co.absa.abris.avro.AvroSerDe._
        
        val kafkaDS = inputDataDS.map(ccb => {
            ccb
        })
        
        val schemaRegistryConfs = Map(
            SchemaManager.PARAM_SCHEMA_REGISTRY_URL -> "http://localhost:8081"
        )
        
        val avroDS = kafkaDS.toDF()
            .toConfluentAvro("CreditCardBalance", "CreditCardBalance", "domain")(schemaRegistryConfs)
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "credit_card_balance_avro4")
            .option("checkpointLocation", "checkpoints/ccBalanceIngest_checkpoints")
            .start()
        
        avroDS.awaitTermination()
    }
    
    
    /**
      * Transforms dataset to kafka applicable format using Avro format and
      * sends data to kafka topic credit-card-balance
      *
      * @param inputDataDS Dataset[CreditCardBalance]
      */
    def transformAndWriteToKafkaAvro(inputDataDS: Dataset[CreditCardBalance]): Unit = {
        
        implicit val kafkaRecordEncoder: sql.Encoder[(Long, Array[Byte])] = Encoders.product[(Long, Array[Byte])]
        
        //transform data to kafka applicable format
        
        val kafkaDS = inputDataDS.map(ccb => {
            val schema = AvroSchema[CreditCardBalance]
            
            val recordInjection: Injection[GenericRecord, Array[Byte]] = GenericAvroCodecs.toBinary(schema)
            
            val format = RecordFormat[CreditCardBalance]
            val record = format.to(ccb)
            val brecord = recordInjection.apply(record)
            
            (ccb.skIdCurr, brecord)
        })(kafkaRecordEncoder)
        
        //write data to kafka topic
        val writer = kafkaDS.selectExpr("CAST (_1 AS STRING) AS key", "_2 AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "credit_card_balance_avro")
            .option("checkpointLocation", "checkpoints/ccBalanceIngest_checkpoints")
            .start()
        
        writer.awaitTermination()
    }
}
