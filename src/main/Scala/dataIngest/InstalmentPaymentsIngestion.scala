package dataIngest

import com.esotericsoftware.minlog.Log.Logger
import config.Settings
import domain.{FileSchemas, InstalmentPayment}
import com.owlike.genson.defaultGenson._
import org.apache.spark.sql.SparkSession
import utils.LoggerUtils


object InstalmentPaymentsIngestion {
    
    def main(args: Array[String]): Unit = {
        LoggerUtils.enableOnlyErrorLogging
        readInstalmentsPaymentsAndSendToKafka()
    }
    
    /**
      * Reads installment payments and sends them to Kafka topic
      */
    def readInstalmentsPaymentsAndSendToKafka(): Unit = {
        val spark = SparkSession.builder()
            .master(Settings.Spark.master)
            .appName("dataIngest")
            .getOrCreate()
        
        import spark.implicits._
        //Read data from csv file
        val inputData = spark
            .readStream
            .format("csv")
            .option("header", "true")
            .schema(FileSchemas.instalmentsPaymentsSchema)
            .load(Settings.DataIngest.instalPaymentsFilesPath)
        
        //transform data to DataSet[CreditCardBalance]
        val inputDataDS = inputData
            .withColumn("skIdPrev", $"SK_ID_PREV".cast("long"))
            .withColumn("skIdCurr", $"SK_ID_CURR".cast("long"))
            .withColumn("numInstalmentVersion", $"NUM_INSTALMENT_VERSION".cast("int"))
            .withColumn("numInstalmentNumber", $"NUM_INSTALMENT_NUMBER".cast("int"))
            .withColumn("daysInstalment", $"DAYS_INSTALMENT".cast("int"))
            .withColumn("daysEntryPayment", $"DAYS_ENTRY_PAYMENT".cast("int"))
            .withColumn("amtInstalment", $"AMT_INSTALMENT".cast("double"))
            .withColumn("amtPayment", $"AMT_PAYMENT".cast("double"))
            .as[InstalmentPayment]
        
        val kafkaDS = inputDataDS.map(ip => {
            (ip.skIdCurr, toJson(ip))
        })
        
        //write data to kafka topic
        val writer = kafkaDS.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "instalment-payment")
            .option("checkpointLocation", "checkpoints/instalIngestion_checkpoints")
            .start()
        
        writer.awaitTermination()
        
    }
    
}
