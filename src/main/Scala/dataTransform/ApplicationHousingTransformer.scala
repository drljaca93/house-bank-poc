package dataTransform

import config.Settings
import domain.{ApplicationHousing, CreditCardBalance}
import org.apache.spark.sql
import org.apache.spark.sql.{Dataset, SparkSession}
import utils.LoggerUtils
import com.owlike.genson.defaultGenson._


object ApplicationHousingTransformer {
    
    def main(args: Array[String]): Unit = {
        LoggerUtils.enableOnlyErrorLogging
    
        //create spark session
        val spark = new sql.SparkSession.Builder()
            .appName("appHousingTransformer")
            .config("spark.master", "local")
            .getOrCreate()
        
        readAppHousingFromTopic(spark)
    }
    
    
    /**
      * Reads data from kafka topic and transforms it to DataSet[ApplicationHousing]
      *
      * @param spark
      * @return
      */
    def readAppHousingFromTopic(spark: SparkSession): Dataset[ApplicationHousing] = {
        
        val appHousingDF = spark
            .read
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("subscribe", "application-housing")
            .option("startingOffsets", "earliest")
            .load()
        
        //ccBalanceDF.printSchema()
        import spark.implicits._
        val appHousingDS = appHousingDF.selectExpr("CAST(value AS STRING)")
            .map(row => {
                val appHousing = fromJson[ApplicationHousing](row(0).toString)
                appHousing
            })
        
        //appHousingDS.printSchema()
        //appHousingDS.show(10)
        appHousingDS
    }
}
