package dataTransform

import config.Settings
import domain.{ApplicationHousing, ApplicationOccupation}
import org.apache.spark.sql
import org.apache.spark.sql.{Dataset, SparkSession}
import utils.LoggerUtils
import com.owlike.genson.defaultGenson._

object ApplicationOccupationTransformer {
    
    val appOccupationTopic = "application-occupation"
    
    def main(args: Array[String]): Unit = {
        LoggerUtils.enableOnlyErrorLogging
        
        //create spark session
        val spark = new sql.SparkSession.Builder()
            .appName("appOccupationTransformer")
            .config("spark.master", "local")
            .getOrCreate()
    
        readAppOccupationFromTopic(spark)
    }
    
    
    /**
      * Reads data from kafka topic and transforms it to DataSet[ApplicationHousing]
      *
      * @param spark
      * @return
      */
    def readAppOccupationFromTopic(spark: SparkSession): Dataset[ApplicationOccupation] = {
        
        val appOccupationDF = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("subscribe", "application-occupation")
            //.option("startingOffsets", "earliest")
            .load()
        
        //ccBalanceDF.printSchema()
        import spark.implicits._
        val appOccupationDS = appOccupationDF.selectExpr("CAST(value AS STRING)")
            .map(row => {
                val appHousing = fromJson[ApplicationOccupation](row(0).toString)
                appHousing
            })
    
        appOccupationDS
    }
}
