package dataTransform

import config.Settings
import domain.{CreditCardAvgBalance, CreditCardBalance}
import org.apache.spark.sql
import org.apache.spark.sql.{Dataset, Encoder, Encoders, SparkSession}
import utils.LoggerUtils
import com.owlike.genson.defaultGenson._
import com.sksamuel.avro4s.{AvroSchema, RecordFormat}
import com.twitter.bijection.Injection
import com.twitter.bijection.avro.GenericAvroCodecs
import org.apache.avro.generic.GenericRecord
import org.apache.spark.sql.streaming.OutputMode
import za.co.absa.abris.avro.read.confluent.SchemaManager
import za.co.absa.abris.avro.schemas.policy.SchemaRetentionPolicies.RETAIN_SELECTED_COLUMN_ONLY

object CCBalanceTransformer {
    
    
    def main(args: Array[String]): Unit = {
        
        LoggerUtils.enableOnlyErrorLogging
        
        //create spark session
        val spark = new sql.SparkSession.Builder()
            .appName("ccBalanceTransformer")
            .master(Settings.Spark.master)
            .getOrCreate()
        
        //val ccBalanceDS = readCCBalanceFromTopic(spark)
        
        //uncomment if you wan to use Avro format from appropriate kafka topic
        //val ccBalanceDS = readCCBalanceAvroFromTopic(spark)
        
        //uncomment if you wan to use Avro format from appropriate kafka topic
        //using schema registry
        //NOTE: you need to have schema registry installed
        val ccBalanceDS = readCCBalanceAvroSchemaRegistryFromTopic(spark)
        
        val aggBalance = calculateAvgCCBalance(ccBalanceDS)
        
        streamCCBalanceAvgToKafka(aggBalance)
        
    }
    
    /**
      * Reads data in avro format from kafka topic and transforms it to DataSet[CreditCardBalance]
      *
      * @param spark SparkSession
      * @return Dataset[CreditCardBalance]
      */
    def readCCBalanceAvroFromTopic(spark: SparkSession): Dataset[CreditCardBalance] = {
        
        val ccBalanceDF = spark
            .read
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("subscribe", "credit_card_balance_avro")
            .option("startingOffsets", "earliest")
            .option("failOnDataLoss", "false")
            .load()
        
        import spark.implicits._
        val ccBalanceDS = ccBalanceDF.selectExpr("CAST(value AS binary)")
            .map(row => {
                
                val schema = AvroSchema[CreditCardBalance]
                val recordInjection: Injection[GenericRecord, Array[Byte]] = GenericAvroCodecs.toBinary(schema)
                
                val record = recordInjection.invert(row(0).asInstanceOf[Array[Byte]]).get
                val format = RecordFormat[CreditCardBalance]
                val ccBalance = format.from(record)
                
                ccBalance
            })
        
        ccBalanceDS
        
    }
    
    
    /**
      * Reads data in avro format from kafka topic and transforms it to DataSet[CreditCardBalance]
      * Uses schema registry for communication
      * see https://github.com/confluentinc/schema-registry for more examples
      *
      * @param spark SparkSession
      * @return Dataset[CreditCardBalance]
      */
    def readCCBalanceAvroSchemaRegistryFromTopic(spark: SparkSession): Dataset[CreditCardBalance] = {
        import za.co.absa.abris.avro.AvroSerDe._
        
        val schemaRegistryConfs = Map(
            SchemaManager.PARAM_SCHEMA_REGISTRY_URL -> "http://localhost:8081",
            SchemaManager.PARAM_SCHEMA_REGISTRY_TOPIC -> "CreditCardBalance",
            SchemaManager.PARAM_VALUE_SCHEMA_ID -> "latest" // set to "latest" if you want the latest schema version to used
        )
        
        import spark.implicits._
        
        val ccBalanceDS = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("subscribe", "credit_card_balance_avro4")
            //.option("startingOffsets", "earliest")
            .fromConfluentAvro("value", None, Some(schemaRegistryConfs))(RETAIN_SELECTED_COLUMN_ONLY)
            .as[CreditCardBalance]
        
        ccBalanceDS
    }
    
    
    /**
      * Reads data from kafka topic and transforms it to DataSet[CreditCardBalance]
      *
      * @param spark SparkSession
      * @return Dataset[CreditCardBalance]
      */
    def readCCBalanceFromTopic(spark: SparkSession): Dataset[CreditCardBalance] = {
        
        val ccBalanceDF = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("subscribe", "credit-card-balance")
            .option("startingOffsets", "earliest")
            .option("failOnDataLoss", "false")
            .load()
        
        import spark.implicits._
        val ccBalanceDS = ccBalanceDF.selectExpr("CAST(value AS STRING)")
            .map(row => {
                val ccBalance = fromJson[CreditCardBalance](row(0).toString)
                ccBalance
            })
        
        ccBalanceDS
    }
    
    /**
      * Aggregate CreditCardBalance dataset to CreditCardAvgBalance dataset
      *
      * @param ccBalanceDS Dataset[CreditCardBalance]
      * @return Dataset[CreditCardAvgBalance]
      */
    def calculateAvgCCBalance(ccBalanceDS: Dataset[CreditCardBalance]): Dataset[CreditCardAvgBalance] = {
        
        implicit val ccAvgBalanceEncoder: Encoder[CreditCardAvgBalance] = Encoders.product[CreditCardAvgBalance]
        
        val aggBalance = ccBalanceDS
            .groupBy(ccBalanceDS.col("skIdCurr"))
            .avg("amtBalance")
            .withColumnRenamed("avg(amtBalance)", "avgAmtBalance").as[CreditCardAvgBalance]
        
        aggBalance
    }
    
    /**
      * Stream ccBalanceAvg to kafka topic
      *
      * @param aggBalance Dataset[CreditCardAvgBalance]
      */
    def streamCCBalanceAvgToKafka(aggBalance: Dataset[CreditCardAvgBalance]): Unit = {
        
        implicit val ccAvgBalanceEncoder: Encoder[(Long, String)] = Encoders.product[(Long, String)]
        
        val kafkaDS = aggBalance.map(ccb => {
            (ccb.skIdCurr, toJson(ccb))
        })
        
        //write data to kafka topic
        val writer = kafkaDS.selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "credit-card-balance-agg")
            .option("failOnDataLoss", "true")
            .outputMode(OutputMode.Update())
            .option("checkpointLocation", "checkpoints/ccBalanceTransform_checkpoints")
            .start()
        
        writer.awaitTermination()
    }
    
    
}
