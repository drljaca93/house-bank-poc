package dataTransform

import config.Settings
import domain._
import org.apache.spark.sql
import org.apache.spark.sql._
import utils.LoggerUtils
import com.owlike.genson.defaultGenson._
import org.apache.spark.sql.streaming.OutputMode


object InstalmentPaymentsTransformer {
    
    def main(args: Array[String]): Unit = {
        LoggerUtils.enableOnlyErrorLogging
        
        //create spark session
        val spark = new sql.SparkSession.Builder()
            .appName("instalmentPaymentsTransformer")
            .config("spark.master", Settings.Spark.master)
            .getOrCreate()
        
        //read instalments
        val instalmentPaymentDS = readInstalmentPaymentsFromTopic(spark)
        
        //read application occupation
        val applicationOccupationDF = ApplicationOccupationTransformer
            .readAppOccupationFromTopic(spark)
            .withColumnRenamed("skIdCurr", "appSkIdCurr")
        
        //join two dataframes
        val joinedDF = instalmentOccupationJoin(instalmentPaymentDS.toDF(), applicationOccupationDF, "inner")
           .dropDuplicates()
        
        streamDataToKafka(joinedDF)
    }
    
    
    /**
      * Reads data from kafka topic and transforms it to DataSet[InstalmentPayment]
      *
      * @param spark
      * @return DataSet[InstalmentPayment]
      */
    def readInstalmentPaymentsFromTopic(spark: SparkSession): Dataset[InstalmentPayment] = {
        
        val instalPaymentsDF = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("subscribe", "instalment-payment")
            .load()
        
        import spark.implicits._
        
        val instalPaymentsDS = instalPaymentsDF.selectExpr("CAST(value AS STRING)")
            .map(row => {
                val instalPayment = fromJson[InstalmentPayment](row(0).toString)
                instalPayment
            })
        
        instalPaymentsDS
    }
    
    
    /**
      * Joins instalmentDF with applicationOccupationDF
      *
      * @param instalmentPaymentDF
      * @param applicationOccupationDF
      * @param joinType
      * @return
      */
    def instalmentOccupationJoin(instalmentPaymentDF: DataFrame, applicationOccupationDF: DataFrame, joinType: String): DataFrame = {
        val joinDF = instalmentPaymentDF
            .join(applicationOccupationDF, instalmentPaymentDF("skIdCurr") === applicationOccupationDF("appSkIdCurr"), joinType)
        
        joinDF
        
    }
    
    /**
      * Streams data to Kafka topic
      *
      * @param joinedDF
      */
    def streamDataToKafka(joinedDF: DataFrame): Unit = {
        
        implicit val ccAvgBalanceEncoder = Encoders.product[(Long, String)]
        
        val kafkaDF = joinedDF.map(ccb => {
            (ccb.getLong(1), ccb.mkString(", "))
        })
        
        
        //Stream data to kafka topic
        val writer = kafkaDF
            .selectExpr("CAST (_1 AS STRING) AS key", "CAST (_2 AS STRING) AS value")
            .writeStream
            .format("kafka")
            .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
            .option("topic", "instalment-occupation")
            .outputMode(OutputMode.Append())
            .option("checkpointLocation", "checkpoints/instalJoin_checkpoints")
            .start()
        
        
        writer.awaitTermination()
    }
    
    def mapRowToKafkaInputFormat(row: Row) : (Long, String) = {
        val retVal = (row.getLong(1), row.mkString(", "))
        
        retVal
    }
    
    
    /**
      * Streams data to CSV file
      * NOTE: Only works with inner join
      *
      * @param joinedDF
      */
    def writeJoinStreamsToCSV(joinedDF: DataFrame): Unit = {
        val writer = joinedDF
            .coalesce(1)
            .writeStream
            .format("csv")
            .option("header", "true")
            .option("checkpointLocation", "checkpoints/instalmentJoin_checkpoints")
            .outputMode(OutputMode.Append())
            .start("file:///home/gdrljaca/BigData/output/joined_output2.csv")
        
        writer.awaitTermination()
    }
    
}
