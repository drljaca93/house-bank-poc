package dataWriter

import config.Settings
import domain.{CreditCardAvgBalance, CreditCardBalance}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import utils.LoggerUtils
import org.apache.spark.sql.execution.datasources.hbase._
import com.owlike.genson.defaultGenson._
import org.apache.spark.sql

object CCAvgBalanceWriter {
    
    
    val ccBalanceAvgCatalog =
        s"""{
           |"table":{"namespace":"default", "name":"CreditCardAvgBalance", "tableCoder":"PrimitiveType"},
           |"rowkey":"key",
           |"columns":{
           |"skIdCurr":{"cf":"rowkey", "col":"key", "type":"int"},
           |"avgAmtBalance":{"cf":"cf1", "col":"avgAmtBalance", "type":"double"}
           |}
           |}""".stripMargin
    
    def main(args: Array[String]): Unit = {
        
        LoggerUtils.enableOnlyErrorLogging
        
        //create spark session
        val spark = new sql.SparkSession.Builder()
          .appName("dataWriter")
          .config("spark.master", "local")
          .getOrCreate()
        
        val ccAvgBalanceDS = readCCAvgBalanceFromTopic(spark)
        
        writeDataToHBase(spark, ccAvgBalanceDS)
    }
    
    /**
      * Reads data from kafka topic and transforms it to DataSet[CreditCardAvgBalance]
      *
      * @param spark
      * @return
      */
    def readCCAvgBalanceFromTopic(spark: SparkSession): Dataset[CreditCardAvgBalance] = {
        
        val ccAvgBalanceDF = spark
          .read
          .format("kafka")
          .option("kafka.bootstrap.servers", Settings.Kafka.bootstraperServers)
          .option("subscribe", "credit-card-balance-agg")
          .option("startingOffsets", "earliest")
          .load()
        
        import spark.implicits._
        
        val ccAvgBalanceDS = ccAvgBalanceDF.selectExpr("CAST(value AS STRING)")
          .map(row => {
              val ccBalance = fromJson[CreditCardAvgBalance](row(0).toString)
              ccBalance
          })
        
        ccAvgBalanceDS
    }
    
    /**
      * Writes ccAvgBalance data to the HBase
      *
      * @param spark
      * @param ccAvgBalancesDC
      */
    def writeDataToHBase(spark: SparkSession, ccAvgBalancesDC: Dataset[CreditCardAvgBalance]): Unit = {
        
        ccAvgBalancesDC.write
          .options(
              Map(HBaseTableCatalog.tableCatalog -> ccBalanceAvgCatalog, HBaseTableCatalog.newTable -> "5"))
          .format("org.apache.spark.sql.execution.datasources.hbase")
          .save()
        
        spark.stop()
        
    }
    
    /**
      * Reads data from Hbase table
      * @param spark
      */
    def readDataFromHbase(spark: SparkSession): Unit = {
        
        val ccAvgBalances = spark
          .read
          .options(
              Map(HBaseTableCatalog.tableCatalog -> ccBalanceAvgCatalog, HBaseTableCatalog.newTable -> "5"))
          .format("org.apache.spark.sql.execution.datasources.hbase")
    }
    
    /**
      * Creates test data
      *
      * @param spark
      * @return
      */
    def createTestData(spark: SparkSession): DataFrame = {
        val cca1 = CreditCardAvgBalance(1, 201.5)
        val cca2 = CreditCardAvgBalance(2, 314.6)
        val cca3 = CreditCardAvgBalance(3, 345.6)
        val cca4 = CreditCardAvgBalance(4, 4324.6)
        
        val list = List(cca1, cca2, cca3, cca4)
        
        val rangeDF = spark.createDataFrame(list).cache()
        
        rangeDF.printSchema()
        rangeDF.show()
        
        rangeDF
    }
    
    
}
