package object domain {
    
    case class BureauBalance(skIdBureau: Long,
                             monthsBalance: Int,
                             status: String)
    
    case class CreditCardBalance(skIdPrev: Long,
                                 skIdCurr: Long,
                                 monthsBalance: Int,
                                 amtBalance: Double
                                 /*amtCreditLimitActual : Long,
                                 amtDrawingsAtmCurrent : Long,
                                 amtDrawingsOtherCurrent: Long,
                                 amtDrawingsPosCurrent: Long,
                                 amtInstMinRegularity: Double,
                                 amtPaymentCurrent: Long,
                                 amtPaymentTotalCurrent: Long,
                                 amtReceivablePrincipal: Double*/)
    
    case class CreditCardAvgBalance(skIdCurr: Long,
                                    avgAmtBalance: Double)
    
    
    case class InstalmentPayment(skIdPrev: Long,
                                 skIdCurr: Long,
                                 numInstalmentVersion: Int,
                                 numInstalmentNumber: Int,
                                 daysInstalment: Int,
                                 daysEntryPayment: Int,
                                 amtInstalment: Double,
                                 amtPayment: Double)
    
    case class ApplicationHousing(skIdCurr: Long,
                                  apartmentsAvg: Option[Double],
                                  basementAreaAvg: Option[Double],
                                  yearsBeginXPluatationAvg: Option[Double],
                                  yearsBuildAvg: Option[Double],
                                  commonAreaAvg: Option[Double],
                                  elevatorsAvg: Option[Double],
                                  entrancesAvg: Option[Double],
                                  floorsmaxAvg: Option[Double],
                                  floorsMinAvg: Option[Double],
                                  landAreaAvg: Option[Double],
                                  livingApartmentsAvg: Option[Double],
                                  livingAreaAvg: Option[Double],
                                  nonLivingApartmentsAvg: Option[Double],
                                  nonLivingAreaAvg: Option[Double],
                                  apartmentsMode: Option[Double],
                                  basementAreaMode: Option[Double]
                                 )
    
    case class ApplicationOccupation(skIdCurr: Long,
                                     codeGender: String,
                                     nameIncomeType: String,
                                     nameEducationType: String,
                                     nameFamilyStatus: String,
                                     occupationType: String,
                                     cntFamMembers: Option[Int]
                                    )
    
}



