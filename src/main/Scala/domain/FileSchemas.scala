package domain

import org.apache.spark.sql.types._

object FileSchemas {
    
    lazy val ccBalanceSchema = new StructType()
        .add("SK_ID_PREV", "string")
        .add("SK_ID_CURR", "string")
        .add("MONTHS_BALANCE", "string")
        .add("AMT_BALANCE", "string")
    
    
    lazy val instalmentsPaymentsSchema = new StructType()
        .add("SK_ID_PREV", "string")
        .add("SK_ID_CURR", "string")
        .add("NUM_INSTALMENT_VERSION", "string")
        .add("NUM_INSTALMENT_NUMBER", "string")
        .add("DAYS_INSTALMENT", "string")
        .add("DAYS_ENTRY_PAYMENT", "string")
        .add("AMT_INSTALMENT", "string")
        .add("AMT_PAYMENT", "string")
    
    
    lazy val SK_ID_CURR = "SK_ID_CURR"
    lazy val TARGET = "TARGET"
    lazy val NAME_CONTRACT_TYPE = "NAME_CONTRACT_TYPE"
    lazy val CODE_GENDER = "CODE_GENDER"
    lazy val FLAG_OWN_CAR = "FLAG_OWN_CAR"
    lazy val FLAG_OWN_REALTY = "FLAG_OWN_REALTY"
    lazy val CNT_CHILDREN = "CNT_CHILDREN"
    lazy val AMT_INCOME_TOTAL = "AMT_INCOME_TOTAL"
    lazy val AMT_CREDIT = "AMT_CREDIT"
    lazy val AMT_ANNUITY = "AMT_ANNUITY"
    lazy val AMT_GOODS_PRICE = "AMT_GOODS_PRICE"
    lazy val NAME_TYPE_SUITE = "NAME_TYPE_SUITE"
    lazy val NAME_INCOME_TYPE = "NAME_INCOME_TYPE"
    lazy val NAME_EDUCATION_TYPE = "NAME_EDUCATION_TYPE"
    lazy val NAME_FAMILY_STATUS = "NAME_FAMILY_STATUS"
    lazy val NAME_HOUSING_TYPE = "NAME_HOUSING_TYPE"
    lazy val REGION_POPULATION_RELATIVE = "REGION_POPULATION_RELATIVE"
    lazy val DAYS_BIRTH = "DAYS_BIRTH"
    lazy val DAYS_EMPLOYED = "DAYS_EMPLOYED"
    lazy val DAYS_REGISTRATION = "DAYS_REGISTRATION"
    lazy val DAYS_ID_PUBLISH = "DAYS_ID_PUBLISH"
    lazy val OWN_CAR_AGE = "OWN_CAR_AGE"
    lazy val FLAG_MOBIL = "FLAG_MOBIL"
    lazy val FLAG_EMP_PHONE = "FLAG_EMP_PHONE"
    lazy val FLAG_WORK_PHONE = "FLAG_WORK_PHONE"
    lazy val FLAG_CONT_MOBILE = "FLAG_CONT_MOBILE"
    lazy val FLAG_PHONE = "FLAG_PHONE"
    lazy val FLAG_EMAIL = "FLAG_EMAIL"
    lazy val OCCUPATION_TYPE = "OCCUPATION_TYPE"
    lazy val CNT_FAM_MEMBERS = "CNT_FAM_MEMBERS"
    lazy val REGION_RATING_CLIENT = "REGION_RATING_CLIENT"
    lazy val REGION_RATING_CLIENT_W_CITY = "REGION_RATING_CLIENT_W_CITY"
    lazy val WEEKDAY_APPR_PROCESS_START = "WEEKDAY_APPR_PROCESS_START"
    lazy val HOUR_APPR_PROCESS_START = "HOUR_APPR_PROCESS_START"
    lazy val REG_REGION_NOT_LIVE_REGION = "REG_REGION_NOT_LIVE_REGION"
    lazy val REG_REGION_NOT_WORK_REGION = "REG_REGION_NOT_WORK_REGION"
    lazy val LIVE_REGION_NOT_WORK_REGION = "LIVE_REGION_NOT_WORK_REGION"
    lazy val REG_CITY_NOT_LIVE_CITY = "REG_CITY_NOT_LIVE_CITY"
    lazy val REG_CITY_NOT_WORK_CITY = "REG_CITY_NOT_WORK_CITY"
    lazy val LIVE_CITY_NOT_WORK_CITY = "LIVE_CITY_NOT_WORK_CITY"
    lazy val ORGANIZATION_TYPE = "ORGANIZATION_TYPE"
    lazy val EXT_SOURCE_1 = "EXT_SOURCE_1"
    lazy val EXT_SOURCE_2 = "EXT_SOURCE_2"
    lazy val EXT_SOURCE_3 = "EXT_SOURCE_3"
    lazy val APARTMENTS_AVG = "APARTMENTS_AVG"
    lazy val BASEMENTAREA_AVG = "BASEMENTAREA_AVG"
    lazy val YEARS_BEGINEXPLUATATION_AVG = "YEARS_BEGINEXPLUATATION_AVG"
    lazy val YEARS_BUILD_AVG = "YEARS_BUILD_AVG"
    lazy val COMMONAREA_AVG = "COMMONAREA_AVG"
    lazy val ELEVATORS_AVG = "ELEVATORS_AVG"
    lazy val ENTRANCES_AVG = "ENTRANCES_AVG"
    lazy val FLOORSMAX_AVG = "FLOORSMAX_AVG"
    lazy val FLOORSMIN_AVG = "FLOORSMIN_AVG"
    lazy val LANDAREA_AVG = "LANDAREA_AVG"
    lazy val LIVINGAPARTMENTS_AVG = "LIVINGAPARTMENTS_AVG"
    lazy val LIVINGAREA_AVG = "LIVINGAREA_AVG"
    lazy val NONLIVINGAPARTMENTS_AVG = "NONLIVINGAPARTMENTS_AVG"
    lazy val NONLIVINGAREA_AVG = "NONLIVINGAREA_AVG"
    lazy val APARTMENTS_MODE = "APARTMENTS_MODE"
    lazy val BASEMENTAREA_MODE = "BASEMENTAREA_MODE"
    lazy val YEARS_BEGINEXPLUATATION_MODE = "YEARS_BEGINEXPLUATATION_MODE"
    lazy val YEARS_BUILD_MODE = "YEARS_BUILD_MODE"
    lazy val COMMONAREA_MODE = "COMMONAREA_MODE"
    lazy val ELEVATORS_MODE = "ELEVATORS_MODE"
    lazy val ENTRANCES_MODE = "ENTRANCES_MODE"
    lazy val FLOORSMAX_MODE = "FLOORSMAX_MODE"
    lazy val FLOORSMIN_MODE = "FLOORSMIN_MODE"
    lazy val LANDAREA_MODE = "LANDAREA_MODE"
    lazy val LIVINGAPARTMENTS_MODE = "LIVINGAPARTMENTS_MODE"
    lazy val LIVINGAREA_MODE = "LIVINGAREA_MODE"
    lazy val NONLIVINGAPARTMENTS_MODE = "NONLIVINGAPARTMENTS_MODE"
    lazy val NONLIVINGAREA_MODE = "NONLIVINGAREA_MODE"
    lazy val APARTMENTS_MEDI = "APARTMENTS_MEDI"
    lazy val BASEMENTAREA_MEDI = "BASEMENTAREA_MEDI"
    lazy val YEARS_BEGINEXPLUATATION_MEDI = "YEARS_BEGINEXPLUATATION_MEDI"
    lazy val YEARS_BUILD_MEDI = "YEARS_BUILD_MEDI"
    lazy val COMMONAREA_MEDI = "COMMONAREA_MEDI"
    lazy val ELEVATORS_MEDI = "ELEVATORS_MEDI"
    lazy val ENTRANCES_MEDI = "ENTRANCES_MEDI"
    lazy val FLOORSMAX_MEDI = "FLOORSMAX_MEDI"
    lazy val FLOORSMIN_MEDI = "FLOORSMIN_MEDI"
    lazy val LANDAREA_MEDI = "LANDAREA_MEDI"
    lazy val LIVINGAPARTMENTS_MEDI = "LIVINGAPARTMENTS_MEDI"
    lazy val LIVINGAREA_MEDI = "LIVINGAREA_MEDI"
    lazy val NONLIVINGAPARTMENTS_MEDI = "NONLIVINGAPARTMENTS_MEDI"
    lazy val NONLIVINGAREA_MEDI = "NONLIVINGAREA_MEDI"
    lazy val FONDKAPREMONT_MODE = "FONDKAPREMONT_MODE"
    lazy val HOUSETYPE_MODE = "HOUSETYPE_MODE"
    lazy val TOTALAREA_MODE = "TOTALAREA_MODE"
    lazy val WALLSMATERIAL_MODE = "WALLSMATERIAL_MODE"
    /*    lazy val EMERGENCYSTATE_MODE = "EMERGENCYSTATE_MODE"
        lazy val OBS_30_CNT_SOCIAL_CIRCLE = "OBS_30_CNT_SOCIAL_CIRCLE"
        lazy val DEF_30_CNT_SOCIAL_CIRCLE = "DEF_30_CNT_SOCIAL_CIRCLE"
        lazy val OBS_60_CNT_SOCIAL_CIRCLE = "OBS_60_CNT_SOCIAL_CIRCLE"
        lazy val DEF_60_CNT_SOCIAL_CIRCLE = "DEF_60_CNT_SOCIAL_CIRCLE"
        lazy val DAYS_LAST_PHONE_CHANGE = "DAYS_LAST_PHONE_CHANGE"
        lazy val FLAG_DOCUMENT_2 = "FLAG_DOCUMENT_2"
        lazy val FLAG_DOCUMENT_3 = "FLAG_DOCUMENT_3"
        lazy val FLAG_DOCUMENT_4 = "FLAG_DOCUMENT_4"
        lazy val FLAG_DOCUMENT_5 = "FLAG_DOCUMENT_5"
        lazy val FLAG_DOCUMENT_6 = "FLAG_DOCUMENT_6"
        lazy val FLAG_DOCUMENT_7 = "FLAG_DOCUMENT_7"
        lazy val FLAG_DOCUMENT_8 = "FLAG_DOCUMENT_8"
        lazy val FLAG_DOCUMENT_9 = "FLAG_DOCUMENT_9"
        lazy val FLAG_DOCUMENT_10 = "FLAG_DOCUMENT_10"
        lazy val FLAG_DOCUMENT_11 = "FLAG_DOCUMENT_11"
        lazy val FLAG_DOCUMENT_12 = "FLAG_DOCUMENT_12"
        lazy val FLAG_DOCUMENT_13 = "FLAG_DOCUMENT_13"
        lazy val FLAG_DOCUMENT_14 = "FLAG_DOCUMENT_14"
        lazy val FLAG_DOCUMENT_15 = "FLAG_DOCUMENT_15"
        lazy val FLAG_DOCUMENT_16 = "FLAG_DOCUMENT_16"
        lazy val FLAG_DOCUMENT_17 = "FLAG_DOCUMENT_17"
        lazy val FLAG_DOCUMENT_18 = "FLAG_DOCUMENT_18"
        lazy val FLAG_DOCUMENT_19 = "FLAG_DOCUMENT_19"
        lazy val FLAG_DOCUMENT_20 = "FLAG_DOCUMENT_20"
        lazy val FLAG_DOCUMENT_21 = "FLAG_DOCUMENT_21"
        lazy val AMT_REQ_CREDIT_BUREAU_HOUR = "AMT_REQ_CREDIT_BUREAU_HOUR"
        lazy val AMT_REQ_CREDIT_BUREAU_DAY = "AMT_REQ_CREDIT_BUREAU_DAY"
        lazy val AMT_REQ_CREDIT_BUREAU_WEEK = "AMT_REQ_CREDIT_BUREAU_WEEK"
        lazy val AMT_REQ_CREDIT_BUREAU_MON = "AMT_REQ_CREDIT_BUREAU_MON"
        lazy val AMT_REQ_CREDIT_BUREAU_QRT = "AMT_REQ_CREDIT_BUREAU_QRT"
        lazy val AMT_REQ_CREDIT_BUREAU_YEAR = "AMT_REQ_CREDIT_BUREAU_YEAR"*/
    
    
    lazy val applicationSchema = new StructType()
        .add(SK_ID_CURR, IntegerType)
        .add(TARGET, IntegerType)
        .add(NAME_CONTRACT_TYPE, StringType)
        .add(CODE_GENDER, StringType)
        .add(FLAG_OWN_CAR, StringType)
        .add(FLAG_OWN_REALTY, StringType)
        .add(CNT_CHILDREN, IntegerType)
        .add(AMT_INCOME_TOTAL, DoubleType)
        .add(AMT_CREDIT, DoubleType)
        .add(AMT_ANNUITY, DoubleType)
        .add(AMT_GOODS_PRICE, DoubleType)
        .add(NAME_TYPE_SUITE, StringType)
        .add(NAME_INCOME_TYPE, StringType)
        .add(NAME_EDUCATION_TYPE, StringType)
        .add(NAME_FAMILY_STATUS, StringType)
        .add(NAME_HOUSING_TYPE, StringType)
        .add(REGION_POPULATION_RELATIVE, DoubleType)
        .add(DAYS_BIRTH, IntegerType)
        .add(DAYS_EMPLOYED, IntegerType)
        .add(DAYS_REGISTRATION, DoubleType)
        .add(DAYS_ID_PUBLISH, IntegerType)
        .add(OWN_CAR_AGE, StringType)
        .add(FLAG_MOBIL, IntegerType)
        .add(FLAG_EMP_PHONE, IntegerType)
        .add(FLAG_WORK_PHONE, IntegerType)
        .add(FLAG_CONT_MOBILE, IntegerType)
        .add(FLAG_PHONE, IntegerType)
        .add(FLAG_EMAIL, IntegerType)
        .add(OCCUPATION_TYPE, StringType)
        .add(CNT_FAM_MEMBERS, DoubleType)
        .add(REGION_RATING_CLIENT, IntegerType)
        .add(REGION_RATING_CLIENT_W_CITY, IntegerType)
        .add(WEEKDAY_APPR_PROCESS_START, StringType)
        .add(HOUR_APPR_PROCESS_START, IntegerType)
        .add(REG_REGION_NOT_LIVE_REGION, IntegerType)
        .add(REG_REGION_NOT_WORK_REGION, IntegerType)
        .add(LIVE_REGION_NOT_WORK_REGION, IntegerType)
        .add(REG_CITY_NOT_LIVE_CITY, IntegerType)
        .add(REG_CITY_NOT_WORK_CITY, IntegerType)
        .add(LIVE_CITY_NOT_WORK_CITY, IntegerType)
        .add(ORGANIZATION_TYPE, StringType)
        .add(EXT_SOURCE_1, DoubleType)
        .add(EXT_SOURCE_2, DoubleType)
        .add(EXT_SOURCE_3, DoubleType)
        .add(APARTMENTS_AVG, DoubleType)
        .add(BASEMENTAREA_AVG, DoubleType)
        .add(YEARS_BEGINEXPLUATATION_AVG, DoubleType)
        .add(YEARS_BUILD_AVG, DoubleType)
        .add(COMMONAREA_AVG, DoubleType)
        .add(ELEVATORS_AVG, DoubleType)
        .add(ENTRANCES_AVG, DoubleType)
        .add(FLOORSMAX_AVG, DoubleType)
        .add(FLOORSMIN_AVG, DoubleType)
        .add(LANDAREA_AVG, DoubleType)
        .add(LIVINGAPARTMENTS_AVG, DoubleType)
        .add(LIVINGAREA_AVG, DoubleType)
        .add(NONLIVINGAPARTMENTS_AVG, DoubleType)
        .add(NONLIVINGAREA_AVG, DoubleType)
        .add(APARTMENTS_MODE, DoubleType)
        .add(BASEMENTAREA_MODE, DoubleType)
        .add(YEARS_BEGINEXPLUATATION_MODE, DoubleType)
        .add(YEARS_BUILD_MODE, DoubleType)
        .add(COMMONAREA_MODE, DoubleType)
        .add(ELEVATORS_MODE, DoubleType)
        .add(ENTRANCES_MODE, DoubleType)
        .add(FLOORSMAX_MODE, DoubleType)
        .add(FLOORSMIN_MODE, DoubleType)
        .add(LANDAREA_MODE, DoubleType)
        .add(LIVINGAPARTMENTS_MODE, DoubleType)
        .add(LIVINGAREA_MODE, DoubleType)
        .add(NONLIVINGAPARTMENTS_MODE, DoubleType)
        .add(NONLIVINGAREA_MODE, DoubleType)
        .add(APARTMENTS_MEDI, DoubleType)
        .add(BASEMENTAREA_MEDI, DoubleType)
        .add(YEARS_BEGINEXPLUATATION_MEDI, DoubleType)
        .add(YEARS_BUILD_MEDI, DoubleType)
        .add(COMMONAREA_MEDI, DoubleType)
        .add(ELEVATORS_MEDI, DoubleType)
        .add(ENTRANCES_MEDI, DoubleType)
        .add(FLOORSMAX_MEDI, DoubleType)
        .add(FLOORSMIN_MEDI, DoubleType)
        .add(LANDAREA_MEDI, DoubleType)
        .add(LIVINGAPARTMENTS_MEDI, DoubleType)
        .add(LIVINGAREA_MEDI, DoubleType)
        .add(NONLIVINGAPARTMENTS_MEDI, DoubleType)
        .add(NONLIVINGAREA_MEDI, DoubleType)
        .add(FONDKAPREMONT_MODE, StringType)
        .add(HOUSETYPE_MODE, StringType)
        .add(TOTALAREA_MODE, DoubleType)
        .add(WALLSMATERIAL_MODE, StringType)
    /*        .add(EMERGENCYSTATE_MODE, StringType)
            .add(OBS_30_CNT_SOCIAL_CIRCLE, StringType)
            .add(DEF_30_CNT_SOCIAL_CIRCLE, DoubleType)
            .add(OBS_60_CNT_SOCIAL_CIRCLE, DoubleType)
            .add(DEF_60_CNT_SOCIAL_CIRCLE, DoubleType)
            .add(DAYS_LAST_PHONE_CHANGE, DoubleType)
            .add(FLAG_DOCUMENT_2, DoubleType)
            .add(FLAG_DOCUMENT_3, IntegerType)
            .add(FLAG_DOCUMENT_4, IntegerType)
            .add(FLAG_DOCUMENT_5, IntegerType)
            .add(FLAG_DOCUMENT_6, IntegerType)
            .add(FLAG_DOCUMENT_7, IntegerType)
            .add(FLAG_DOCUMENT_8, IntegerType)
            .add(FLAG_DOCUMENT_9, IntegerType)
            .add(FLAG_DOCUMENT_10, IntegerType)
            .add(FLAG_DOCUMENT_11, IntegerType)
            .add(FLAG_DOCUMENT_12, IntegerType)
            .add(FLAG_DOCUMENT_13, IntegerType)
            .add(FLAG_DOCUMENT_14, IntegerType)
            .add(FLAG_DOCUMENT_15, IntegerType)
            .add(FLAG_DOCUMENT_16, IntegerType)
            .add(FLAG_DOCUMENT_17, IntegerType)
            .add(FLAG_DOCUMENT_18, IntegerType)
            .add(FLAG_DOCUMENT_19, IntegerType)
            .add(FLAG_DOCUMENT_20, IntegerType)
            .add(FLAG_DOCUMENT_21, IntegerType)
            .add(AMT_REQ_CREDIT_BUREAU_HOUR, IntegerType)
            .add(AMT_REQ_CREDIT_BUREAU_DAY, DoubleType)
            .add(AMT_REQ_CREDIT_BUREAU_WEEK, DoubleType)
            .add(AMT_REQ_CREDIT_BUREAU_MON, DoubleType)
            .add(AMT_REQ_CREDIT_BUREAU_QRT, DoubleType)
            .add(AMT_REQ_CREDIT_BUREAU_YEAR, DoubleType)
        */
}
