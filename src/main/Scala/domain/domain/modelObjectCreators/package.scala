package domain.domain

import domain.CreditCardBalance
import org.apache.spark.sql.Row
import scala.util.Try


package object modelObjectCreators {
    
    def createCreditCardBalanceObject(row: String): CreditCardBalance = {
        
        val splits = row.split(",")
        val creditCardBalance = new CreditCardBalance(splits(0).toLong, splits(1).toLong,splits(2).toInt,
            splits(3).toDouble)
        
        creditCardBalance
        
    }
}

/**
  * , splits(4).toLong, splits(5).toLong, splits(6).toLong, splits(7).toLong,
  * splits(8).toDouble, splits(9).toLong, splits(10).toLong, splits(11).toLong
  */
