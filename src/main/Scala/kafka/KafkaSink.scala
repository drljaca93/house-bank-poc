package kafka

import java.util.Properties
import collection.JavaConversions._
import org.apache.kafka.clients.producer._


/**
  * KafkaSink is used in order to achieve kafka consumer serialization
  * so ti can be distributed
  *
  * @param createProducer
  */
class KafkaSink(createProducer: () => KafkaProducer[String, String]) extends Serializable {
    
    lazy val producer = createProducer()
    
    def send(record: ProducerRecord[String, String]): Unit = producer.send(record)
    
    def send(record: ProducerRecord[String, String], callback: Callback): Unit = producer.send(record, callback)
}

object KafkaSink {
    def apply(config: Properties): KafkaSink = {
        val f = () => {
            val producer = new KafkaProducer[String, String](config)
            sys.addShutdownHook {
                producer.close()
            }
            producer
        }
        new KafkaSink(f)
    }
    
    
    def buildFromMap(properties: Map[String, Object]) =
        (new Properties /: properties) {
            case (a, (k, v)) =>
                a.put(k, v)
                a
        }
}