package utils

import org.apache.log4j.{Level, Logger}

object LoggerUtils {
    
    /**
      * Enables only error logging
      */
    def enableOnlyErrorLogging: Unit = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)
    }
}

