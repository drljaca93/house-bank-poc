package utils

import java.lang.management.ManagementFactory

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object SparkUtils {
    val isIDE = {
        ManagementFactory.getRuntimeMXBean.getInputArguments.toString.contains("IntelliJ IDEA")
    }
    
    def getSparkContext(appName: String) = {
        
        var checkPointDirectory = ""
        
        // get spark configuration
        val conf = new SparkConf()
          .setAppName(appName)
          .setMaster("local[*]")
        
        // Check if running from IDE
       /* if (isIDE) {
            System.setProperty("hadoop.home.dir", "F:\\Libraries\\WinUtils") // required for winutils
            conf.setMaster("local[*]")
            checkPointDirectory = "file:///c:/temp"
        } else {
            checkPointDirectory = "hdfs://lambda-pluralsight:9000/spark/checkpoint"
        }*/
        
        // setup spark context
        val sc = SparkContext.getOrCreate(conf)
        sc.setCheckpointDir(checkPointDirectory)
        sc
    }
    
}


